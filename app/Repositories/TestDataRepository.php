<?php namespace App\Repositories;

use App\Repositories\Contracts\TestDataRepositoryInterface;

class TestDataRepository implements TestDataRepositoryInterface
{
    private ?array $decoded = null;

    public function getCars(): array
    {
        return $this->_getFromFile('cars');
    }

    public function getPeople(): array
    {
        return $this->_getFromFile('people');
    }

    public function getNumbers(): array
    {
        return $this->_getFromFile('numbers');
    }

    public function getPhoneNumbers(): array
    {
        return $this->_getFromFile('phone-numbers');
    }

    private function _getFromFile(string $key): array
    {
        if ( ! $this->decoded ) {
            $this->decoded = json_decode(file_get_contents(resource_path('test-data/test-data.json')), true, 512, JSON_THROW_ON_ERROR);
        }

        return $this->decoded[ $key ] ?? [];
    }
}
