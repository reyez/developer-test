<?php namespace App;

use App\Repositories\Contracts\TestDataRepositoryInterface;

class Test implements TestInterface
{
    protected TestDataRepositoryInterface $testDataRepository;

    public function __construct(TestDataRepositoryInterface $testDataRepository)
    {
        $this->testDataRepository = $testDataRepository;
    }

    public function returnFullNameOfPeopleWithGender(string $gender): array
    {
        // TODO: Implement returnPeopleOfGender() method.
    }

    public function returnAllBrandNamesWithModelContaining(string $string): array
    {
        // TODO: Implement returnAllCarModelsContaining() method.
    }

    public function returnSumAllNumbersGreaterOrEqualThan(int $number): int
    {
        // TODO: Implement returnAllNumbersGreaterOrEqualThan() method.
    }

    public function returnPhoneNumberEndingIn(string $string): array
    {
        // TODO: Implement returnPhoneNumberEndingIn() method.
    }
}
